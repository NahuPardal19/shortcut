using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class START_GAME : MonoBehaviour
{
    bool IsStart;
    [SerializeField] List<GameObject> listaObjetos = new List<GameObject>();
    [SerializeField] GameObject tile_inicio;
    [SerializeField] GameObject tile_juego;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.N)) && !IsStart)
        {
            tile_inicio.SetActive(false);
            tile_juego.SetActive(true);
            foreach (GameObject g in listaObjetos)
            {
                if (g.name == "Tutorial" || g.name == "Door")
                {
                    g.SetActive(true);
                }
                else
                {
                    PlayerMovement pm = g.GetComponent<PlayerMovement>();
                    if (pm)
                    {
                        pm.enabled = true;
                    }
                    g.GetComponent<Collider2D>().enabled = true;
                    g.GetComponent<SpriteRenderer>().enabled = true;
                    IsStart = true;
                    g.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                }
            }
        }
    }
}
