using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{
    public AnimationClip exitAnimation;
    public AnimationClip enterAnimation;

    private void Start()
    {
        StartAnimation();
    }

    private void ChangeLevel()
    {
        GameManager.PasarNivel();
    }

    public void StartExitAnimation()
    {
        GetComponent<Animation>().clip = exitAnimation;
        GetComponent<Animation>().Play();
    }

    private void StartAnimation()
    {
        GetComponent<Animation>().clip = enterAnimation;
        GetComponent<Animation>().Play();
    }
}
