using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class copiarZoom : MonoBehaviour
{
    public float escala = 0;
    public GameObject objetoReescalar;

    public void Update()
    {
        escala = ShortcutManager.zoom;
        
        Vector3 escalaActual = objetoReescalar.transform.localScale;

        escalaActual.x = escala;

        float proporcion = escala / escalaActual.x;
        escalaActual.y *= proporcion;
        escalaActual.z *= proporcion;

        objetoReescalar.transform.localScale = escalaActual;
    }
}