using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivarObjetoCuando : MonoBehaviour
{
    public bool JugadorHizoTrigger = false;
    public GameObject ObjetoAEliminar;

    private void Start()
    {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Player"))
        {
            JugadorHizoTrigger = true;
            ObjetoAEliminar.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("Player"))
        {
            JugadorHizoTrigger = false;
            ObjetoAEliminar.SetActive(true);
        }
    }
}
