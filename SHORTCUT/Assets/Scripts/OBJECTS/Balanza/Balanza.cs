using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balanza : MonoBehaviour
{
    public bool BalanzaDe1Jugador = false;

    public static bool BalanzaCompletada = false;

    public Sprite spriteBalanza0;
    public Sprite spriteBalanza1;
    public Sprite spriteBalanza2;
    public Sprite spriteBalanza3;

    public GameObject collider1player;

    public static int numeroBalanza;
    private int anteriorNumeroBalanza = 0;

    public GameObject ObjetoAEliminar;

    #region  Vectores de los Colliders

    private Vector2 _boxcollider0Size;
    private Vector2 _boxcollider1Size;
    private Vector2 _boxcollider2Size;
    private Vector2 _boxcollider3Size;
    
    private Vector2 _boxcollider0Offset;
    private Vector2 _boxcollider1Offset;
    private Vector2 _boxcollider2Offset;
    private Vector2 _boxcollider3Offset;

    #endregion
    
    void Start()
    {
        BalanzaCompletada = false;

        ChequeoCambiarTamaño();
        
        Sprite spriteActual = GetComponent<SpriteRenderer>().sprite;
        
        _boxcollider0Size = GetComponent<BoxCollider2D>().size;
        _boxcollider0Offset = GetComponent<BoxCollider2D>().offset;
        
        _boxcollider1Size = new Vector2(2.423938f, 2.729725f);
        _boxcollider1Offset = new Vector2(-0.00523138f, -0.6342724f);
        
        _boxcollider2Size = new Vector2(2.423938f, 2.476363f);
        _boxcollider2Offset = new Vector2(-0.00523138f, -0.7609531f);

        _boxcollider3Size = new Vector2(2.4239378f, 2.34054899f);
        _boxcollider3Offset = new Vector2(-0.00523138046f,-0.828860283f);

        this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza0;
        this.GetComponent<BoxCollider2D>().size = _boxcollider0Size;
        this.GetComponent<BoxCollider2D>().offset = _boxcollider0Offset;


    }
    void Update()
    {
        if (anteriorNumeroBalanza == numeroBalanza) return;
        ChequeoCambiarTamaño();
        anteriorNumeroBalanza = numeroBalanza;
    }

    public void ChequeoCambiarTamaño()
    {
        if (!BalanzaCompletada)
        {
            if (!BalanzaDe1Jugador)
            {
                if (numeroBalanza == 0)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza0;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider0Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider0Offset;
                }
                else if (numeroBalanza == 1)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza1;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider1Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider1Offset;
                }
                else if (numeroBalanza == 2)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza2;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider2Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider2Offset;
                }
                else if (numeroBalanza == 3)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza3;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider3Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider3Offset;
                    Balanza.BalanzaCompletada = true;
                }
            }
            else if (BalanzaDe1Jugador)
            {
                if (numeroBalanza == 0)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza0;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider0Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider0Offset;

                    if (ObjetoAEliminar != null)
                    {
                        ObjetoAEliminar.SetActive(true);
                    }
                    if(collider1player != null) collider1player.SetActive(true);
                }
                else
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza3;
                    this.GetComponent<BoxCollider2D>().size = _boxcollider3Size;
                    this.GetComponent<BoxCollider2D>().offset = _boxcollider3Offset;

                    if (ObjetoAEliminar != null)
                    {
                        ObjetoAEliminar.SetActive(false);
                    }

                    if (collider1player != null) collider1player.SetActive(false);
                }
            }
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteBalanza3;
            this.GetComponent<BoxCollider2D>().size = _boxcollider3Size;
            this.GetComponent<BoxCollider2D>().offset = _boxcollider3Offset;
        }
    }
}
