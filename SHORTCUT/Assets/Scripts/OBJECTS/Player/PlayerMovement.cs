using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private bool ObjetoInteractuable;

    [Header("Movimiento")]
    public float direccion = 0f;
    public float speed = 5f;
    [Range(0, 0.3f)] public float suavizado;
    private Vector3 velocidad = Vector3.zero;
    private bool LookDerecha;
    public ParticleSystem dust;

    [Header("Salto")]
    public float fuerzaSalto;
    public bool estaEnPiso;
    [SerializeField] private LayerMask capa;
    [SerializeField] private float LineaSalto = 1.6f;
    private bool isJumping;

    [Header("Coyote Time")]
    //coyote time
    [SerializeField] private float coyoteTime;
    private float coyoteTimeCounter;
    private bool coyote;

    [Header("Input buffer")]
    //Input Buffer
    [SerializeField] private float jumpBufferTime;
    private float jumpBufferCounter;

    private Animator animator;
    private bool HayAnimator = false;
    

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        estaEnPiso = false;
        animator = GetComponent<Animator>();
        if (animator != null)
        {
            HayAnimator = true;
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) ||
            Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) ||
            Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        {
            direccion = 0f;
        }
        else
        {
            // Permitir movimiento normal si no se están presionando
            if (ObjetoInteractuable)
                direccion = Input.GetAxisRaw("Horizontal") * speed;
            else
                direccion = Input.GetAxisRaw("HorizontalASWD") * speed;

        }

        Salto();

        if (HayAnimator) // animaciones
        {
            if (direccion != 0 && estaEnPiso)
            {
                animator.SetBool("IsRunning", true);
            }
            else if (direccion == 0 && estaEnPiso)
            {
                animator.SetBool("IsRunning", false);
            }

            if (isJumping && estaEnPiso)
            {
                animator.SetBool("IsJumping",false);
                isJumping = false;
            }
        }
        
    }
    private void FixedUpdate()
    {
        MoverPersonaje();
        
    }
    private void MoverPersonaje()
    {
        Vector3 velocidadObjetivo = new Vector2(direccion * Time.deltaTime, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizado);

        //Sprite flip

        if (direccion < 0 && !LookDerecha) // Si la dirección es mayor que 0 y no está mirando a la derecha, voltear
        {
            LookDerecha = true;
            dust.Play();
            FlipSprite();
        }
        else if (direccion > 0 && LookDerecha) // Si la dirección es menor que 0 y está mirando a la derecha, voltear
        {
            LookDerecha = false;
            dust.Play();
            FlipSprite();
        }
        
    }

    private void FlipSprite()
    {
        // Voltear el sprite
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    private void Salto()
    {

        Debug.DrawLine(transform.position + Vector3.down * 0.7f, transform.position + Vector3.right * 0f + Vector3.down * LineaSalto, Color.yellow);

        if (HayAnimator)
        {
            animator.SetBool("IsJumping", true);
            isJumping = true;
        }

        if (Physics2D.Raycast(transform.position + Vector3.down * 0.7f, Vector2.down, LineaSalto, capa))
        {
            estaEnPiso = true;
            coyoteTimeCounter = coyoteTime;
        }
        else
        {
            estaEnPiso = false;
            coyoteTimeCounter -= Time.deltaTime;
        }

        if (ObjetoInteractuable)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.I))
            {
                jumpBufferCounter = jumpBufferTime;
            }
            else
            {
                jumpBufferCounter -= Time.deltaTime;
            }

            if (jumpBufferCounter > 0 && coyoteTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
                jumpBufferCounter = 0;

                // Emitir partículas al saltar
                dust.Play();
            }
            if (Input.GetKeyUp(KeyCode.Space) && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
                coyoteTimeCounter = 0;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                jumpBufferCounter = jumpBufferTime;
            }
            else
            {
                jumpBufferCounter -= Time.deltaTime;
            }

            if (jumpBufferCounter > 0 && coyoteTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
                jumpBufferCounter = 0;

                // Emitir partículas al saltar
                dust.Play();
            }
            if (Input.GetKeyUp(KeyCode.Space) && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
                coyoteTimeCounter = 0;
            }
        }


    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision != null)
        {
            GameObject objetoColisionado = collision.gameObject;
            IbotonPresionable ibotonPresionable = objetoColisionado.GetComponent<IbotonPresionable>();

            if (ibotonPresionable != null)
            {
                ibotonPresionable.onPress();
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other != null)
        {
            GameObject objetoColisionado = other.gameObject;
            IbotonPresionable ibotonPresionable = objetoColisionado.GetComponent<IbotonPresionable>();

            if (ibotonPresionable != null)
            {
                ibotonPresionable.onExit();
            }
        }
    }
}
