using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrera : MonoBehaviour
{
    public GameObject boton;
    private IbotonPresionable interfazBoton;
    public static bool isButtonPressed;
    private void Start()
    {
        interfazBoton = boton.GetComponent<IbotonPresionable>();
    }

    private void Update()
    {
        if (interfazBoton != null)
        {
            if (interfazBoton.isPressed)
            {
                isButtonPressed = true;
            }
            else
            {
                isButtonPressed = false;
            }
        }
       
    }
}
