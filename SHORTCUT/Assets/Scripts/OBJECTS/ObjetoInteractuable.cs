using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjetoInteractuable : MonoBehaviour
{
    //Si el objeto esta siendo agarrado
    private bool ObjetoAgarrado;

    //Nombre con el q accederemos a el por el control f
    public string Nombre;

    [Header("SPRITES")]

    public Sprite Sprite;
    public Sprite SpriteOutline;

    /*  [HideInInspector]*/
    public List<EstadoObjeto> ListaDeEstados;

    public  bool estaControlX;


    void Start()
    {
 
    }

    void Update()
    {
        //Si el objeto no esta siendo agarrado lo paso a su layer normal

    }


    #region METODOS MOUSE

    private void OnMouseDown()
    {
        //Si lo clickeo le doy su rotacion normal y lo selecciono (puedo seleccionar con control o sin control y eso cambiara la manera de agarrarlo)
        if (GameManager.EstadoMouse)
        {
            if (tag != "Player") transform.rotation = Quaternion.identity;
            if (ShortcutManager.objetoSeleccionados.Count < 1)
            {
                if (Input.GetKey(KeyCode.LeftControl)) ShortcutManager.SeleccionarOtroObjetoMas(gameObject);
                else ShortcutManager.SeleccionarObjeto(gameObject);
            }
        }
    }
    void OnMouseDrag()
    {
        if (GameManager.EstadoMouse)
        {
            //Si drageo al objeto lo muevo a su respectiva manera y le cambio su layer a objeto para que no colisione con el jugador
            if (ShortcutManager.objetoSeleccionados.Count > 1)
            {
                ShortcutManager.MoverObjeto();
            }
            else
            {
                ShortcutManager.MoverObjeto(gameObject);
                gameObject.layer = LayerMask.NameToLayer("Object");
            }
        }


        ShortcutManager.PuedeControlZ = false;
    }
    void OnMouseUp()
    {
        if (GameManager.EstadoMouse)
        {
            //Cosa tuya
            if (ObjetoAgarrado)
            {
                if (this.gameObject.transform.CompareTag("Puerta"))
                {
                    this.gameObject.GetComponent<Collider2D>().isTrigger = true;
                }
                //Si suelto el boton del mouse lo dejo de agarrar 
                ObjetoAgarrado = false;
            }

            //Le bajo su velocidad a zero
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            //Puede hacer control z ya que no tiene un objeto agarrado
            ShortcutManager.PuedeControlZ = true;

            //Esto es para un bug asi que no t preocupes
            if (ShortcutManager.objetoSeleccionados.Count > 0)
            {
                if (Input.GetKey(KeyCode.LeftControl)) ShortcutManager.SeleccionarOtroObjetoMas(gameObject);
                else
                    ShortcutManager.SeleccionarObjeto(gameObject);
            }

            //Guardo un estado a todos los objetos porque estoy cambiandole la posicion 
            GameManager.GuardarEstados();
        }
    }
    private void OnMouseOver()
    {
        if (GameManager.EstadoMouse)
        {
            //Si paso el mouse por arriba , no lo selecciono pero le dejo el outline
            GetComponent<SpriteRenderer>().sprite = SpriteOutline;
        }
    }
    private void OnMouseExit()
    {
        if (GameManager.EstadoMouse)
        {
            //Si suelto el mouse arriba de este le saco el outline (no se lo saco si el objeto fue clickeado)
            GetComponent<SpriteRenderer>().sprite = Sprite;
        }
    }

    #endregion



}
