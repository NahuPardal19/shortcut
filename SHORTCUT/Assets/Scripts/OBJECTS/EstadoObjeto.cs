using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoObjeto : MonoBehaviour
{
    public Vector3 posicion;
    public Quaternion rotacion;
    public bool estado;

    public EstadoObjeto()
    {
    }

    public EstadoObjeto(Vector3 posicion, Quaternion rotacion, bool estado)
    {
        this.posicion = posicion;
        this.rotacion = rotacion;
        this.estado = estado;
    }
}
