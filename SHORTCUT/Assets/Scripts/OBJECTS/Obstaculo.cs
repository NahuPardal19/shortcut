using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo : MonoBehaviour
{
    private BoxCollider2D boxcol;
    private List<GameObject> objetosHijos;
    public static bool balanzaAbierta;

    private void Start()
    {
        boxcol = GetComponent<BoxCollider2D>();
        objetosHijos = new List<GameObject>();
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject hijo = transform.GetChild(i).gameObject;
            if(hijo != null)
            objetosHijos.Add(hijo);
        }
    }

    void Update()
    {
        if (objetosHijos != null)
        {
            switch (boxcol.enabled)
            {
                case false:
                {
                    foreach (GameObject hijo in objetosHijos)
                    {
                        hijo.SetActive(false);
                    }

                    break;
                }
                case true:
                {
                    foreach (GameObject hijo in objetosHijos)
                    {
                        if (!hijo.activeSelf)
                        {
                            hijo.SetActive(true);
                        }
                    }

                    break;
                }
            }
        }
    }
}