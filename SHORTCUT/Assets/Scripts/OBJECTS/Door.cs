using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool Abierta = false;
    [SerializeField] private Sprite sprite;
    public GameObject Transition;

    private void Update()
    {
        if (Abierta) GetComponent<SpriteRenderer>().sprite = sprite;
    }
    public void AbrirPuerta()
    {
        Abierta = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && Abierta)
        {
            collision.GetComponent<PlayerMovement>().speed = 0;
            Debug.Log("pasarNivel: " + collision.name);
            Transition transition = Transition.GetComponent<Transition>();
            transition.StartExitAnimation();
        }
    }

}
