using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEditor;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    //Color que siempre tendra el material menos en el control x (son dos porque uno es para poder acceder a la variable estatica desde el inspector)

    [SerializeField] private Color materialObjetosInteractuable;
    public static Color materialObjetosInteractuableStatic;

    [SerializeField] private int ObjetosInteractuablesMaximos;
    public static int ObjetosInteractuablesMaximosStatic;

    [SerializeField] private bool MouseActivado;
    public static bool EstadoMouse;
    private GameObject Menu;
    private GameObject PostProcessing;

    #region PLAYER
    public static GameObject JugadorGameObject;

    private static float velocidadInicial;
    private static float fuerzaSaltoInicial;
    #endregion

    #region Propiedades Camara

    [Header("LIMITAR PANTALLA")]


    [SerializeField] private float margen = 1.0f; // Margen de la c�mara
    [SerializeField] private bool QuieroLimitar;



    public static CinemachineVirtualCamera virtualCamera;


    private float minX, maxX, minY, maxY;
    private float camaraSizeX, camaraSizeY;

    private static CinemachineBasicMultiChannelPerlin CinemachineBasicMultiChannelPerlin;
    private static float tiempoMovimiento;
    private static float tiempoMovimientoTotal;
    private static float intensidadInicial;

    #endregion

    #region Propiedades de Control + A
    //GameObject que tiene todas las monedas
    private GameObject ContenedorMonedas;
    private TextMeshProUGUI textoControlA;
    private float time;
    [HideInInspector] public bool timerActivado;
    #endregion

    #region Propiedades Control +Z

    private ContadorCubos contadorCubos2;


    #endregion

    #region Propiedades Control + F

    private ContadorCubos contadorCubos;


    #endregion

    #region Propiedades de guardar estados para Control + Z

    //Cantidad de control Z que se pueden hacer para poder regularlos (cada vez que cambio de estado a todos los objetos interactuables se agrega uno) -> Mover , Eliminar , Agregar un objeto agregan uno y hacer control z resta uno
    public static int CantidadEstados;
    public static int PosicionActual;
    #endregion

    #region Start & Uptade & Awake

    private void Awake()
    {
        virtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
        CantidadEstados = 0;
        PosicionActual = 0;
    }

    void Start()
    {
        ActivarMouse(false);
        //else ActivarMouse(false);

        PostProcessing = GameObject.Find("Global Volume");
        Menu = GameObject.Find("MENU");
        PostProcessing.SetActive(false);
        Menu.SetActive(false);

        JugadorGameObject = GameObject.FindGameObjectWithTag("Player");


        materialObjetosInteractuableStatic = materialObjetosInteractuable;
        ObjetosInteractuablesMaximosStatic = ObjetosInteractuablesMaximos;

        velocidadInicial = JugadorGameObject.GetComponent<PlayerMovement>().speed;
        fuerzaSaltoInicial = JugadorGameObject.GetComponent<PlayerMovement>().fuerzaSalto;


        CinemachineBasicMultiChannelPerlin = FindObjectOfType<CinemachineBasicMultiChannelPerlin>();

        //Para que al reiniciar la escena no pueda hacer un control Z ya que sino establezco este numero podra hacer control z al reinciar al reiniciar la escena
        GuardarEstados();
        PosicionActual--;

        InicializarNivelControlA();
    }

    void Update()
    {
        Pausar();
        if (QuieroLimitar)
        {
            CalcularLimitesCamara();
            LimitarJugador();

        }

        //Contiene todo los scripts que sean sobre niveles
        ManagerNiveles();

        ModificacionVibracion();
    }

    #endregion

    #region Metodos generales
   
    private void Pausar()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Menu.activeInHierarchy)
            {
                Menu.SetActive(false);
                PostProcessing.SetActive(false);
                if (ActivarAltF4.CanMoveAltF4)
                {
                    MovimientoJugador(true);
                }
                    ShortcutManager.Can_Shortcut = true;
                ActivarMouse(false);
            }
            else
            {
                Menu.SetActive(true);
                PostProcessing.SetActive(true);
                MovimientoJugador(false);
                ShortcutManager.Can_Shortcut = false;
                ActivarMouse(true);
            }
        }
    }
    
    public static void ActivarMouse(bool act)
    {
        if (!act)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            EstadoMouse = act;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            EstadoMouse = act;
        }
    }

    public static List<GameObject> getObjetosInteractuar()
    {
        //Busca todos los objetos que contenta el script "Objeto Interactuable" y los añade a una lista
        List<ObjetoInteractuable> Lista = FindObjectsOfType<ObjetoInteractuable>().ToList();
        List<GameObject> ListaGameObjects = new List<GameObject>();

        //Agarra todos los GameObjects que esten activos en el mapa y retorna una lista con ellos
        foreach (ObjetoInteractuable g in Lista)
        {
            if (g.GetComponent<ObjetoInteractuable>().isActiveAndEnabled)
                ListaGameObjects.Add(g.gameObject);
        }

        return ListaGameObjects;
    }

    public static void PasarNivel()
    {
        //Pasa siempre al siguiente nivel
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public static void ReiniciarNivel()
    {
        //Reinicia siempre tu mismo nivel

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public static void MovimientoJugador(bool movimientoJugador)
    {
        foreach (PlayerMovement p in FindObjectsOfType<PlayerMovement>())
        {
            if (movimientoJugador)
            {
                p.fuerzaSalto = fuerzaSaltoInicial;
                p.speed = velocidadInicial;
            }
            else
            {
                p.fuerzaSalto = 0;
                p.speed = 0;
            }


        }
    }

    #endregion

    #region Metodos de Niveles

    //Metodo General que esta en el update
    private void ManagerNiveles()
    {
        ManagementLevel2();
        ManagementLevel4();
        ManagementLevel8();
        ManagementLevel9();
        ManagementLevelControlF();
    }


    private void ManagementLevel2()
    {
        //Se fija que este en la escena del nivel
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            //Si el contador llega a 0 desactiva el obstaculo que no nos deja pasar
            GameObject g = GameObject.Find("Obstaculo");

            if (FindObjectOfType<ContadorCubos>().contador == 3)
            {
                if (g != null)
                {
                    g.GetComponent<Animator>().SetBool("Open" , true);
                    g.GetComponent<Animator>().SetBool("Close" , false);
                    Balanza.BalanzaCompletada = true;
                }
            }
            //else
            //{
            //    if (g != null)
            //    {
            //        g.GetComponent<Animator>().SetBool("Open" , false);
            //        g.GetComponent<Animator>().SetBool("Close" , true);
            //    }
            //}
        }
    }

    private void ManagementLevel4()
    {
        //Se fija que este en la escena del nivel
        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            //Si el contador llega a 0 desactiva el obstaculo que no nos deja pasar
            GameObject g = GameObject.Find("Obstaculo");
            if (ContadorCubos.contadorStatic == 1)
            {
                if (g != null)
                {
                    Debug.Log("llego a 1");
                    g.SetActive(false);
                    Balanza.BalanzaCompletada = true;
                }
            }

        }
    }

    private void ManagementLevel8()
    {
        if (ContenedorMonedas != null)
        {
            if (timerActivado)
            {
                //Comienza el timer y activa todas las monedas

                time -= Time.deltaTime;

                ContenedorMonedas.SetActive(true);

                //Si se termina el tiempo se reinicia el nivel

                if (time <= 0)
                {
                    textoControlA.text = "";
                    //time = 10;
                    GameManager.ReiniciarNivel();
                }

                //Texto que nos pide eliminar todas las monedas junto con el timer

                textoControlA.text = $"NO COINS IN {Mathf.RoundToInt(time)}";

                //MONEDAS TIENEN QUE TENER UN CIRCLE COLLIDER para poder buscar la cantidad de monedas activas ya que al llegar a 0 tendria que abrirse la puerta

                List<CircleCollider2D> Lista = FindObjectsOfType<CircleCollider2D>().ToList();
                List<GameObject> ListaMonedasActivas = new List<GameObject>();

                foreach (CircleCollider2D g in Lista)
                {
                    if (g.GetComponent<CircleCollider2D>().isActiveAndEnabled)
                        ListaMonedasActivas.Add(g.gameObject);
                }

                //Si la cantidad de moneda es = 0 pasar nivel

                print(ListaMonedasActivas.Count);
                if (ListaMonedasActivas.Count == 0)
                {
                    timerActivado = false;
                    GameObject obstaculos = GameObject.Find("Obstaculos");
                    obstaculos.transform.position = obstaculos.transform.position - (Vector3.down * 7);
                    textoControlA.text = $"Thanks! :)";
                }
            }
        }



    }
    private void InicializarNivelControlA()
    {
        //Busca todas las monedas y las desactiva

        ContenedorMonedas = GameObject.Find("Monedas");
        if (ContenedorMonedas != null)
        {
            ContenedorMonedas.SetActive(false);
            textoControlA = GameObject.Find("ContadorTxt").GetComponent<TextMeshProUGUI>();
            time = 5f;
        }
    }
    private void ManagementLevel9()
    {
        if (SceneManager.GetActiveScene().buildIndex == 7)
        {
            GameObject g = GameObject.Find("Obstaculo");
            if (ContadorCubos.contadorStatic == 3)
            {
                if (g != null)
                {
                    Debug.Log("llego a 1");
                    g.SetActive(false);
                    Balanza.BalanzaCompletada = true;
                }
            }
        }
    }

    private void ManagementLevelControlF()
    {
        if (SceneManager.GetActiveScene().buildIndex == 9)
        {
            contadorCubos = GameObject.Find("Trigger").GetComponent<ContadorCubos>();
            //Si el contador llega a 0 desactiva el obstaculo que no nos deja pasar
            GameObject g = GameObject.Find("Obstaculo");

            if (contadorCubos != null)
            {
                if (contadorCubos.contador == contadorCubos.Maximo)
                {
                    if (g != null)
                    {
                        g.SetActive(false);
                        Destroy(GameObject.Find("Trigger").GetComponent<ContadorCubos>());
                    }
                }
            }
        }
    }


    #endregion

    #region Metodos LimitarJugador

    private void CalcularLimitesCamara()
    {

        camaraSizeY = virtualCamera.m_Lens.OrthographicSize;
        camaraSizeX = camaraSizeY * virtualCamera.m_Lens.Aspect;

        minX = virtualCamera.transform.position.x - camaraSizeX + margen;
        maxX = virtualCamera.transform.position.x + camaraSizeX - margen;
        minY = virtualCamera.transform.position.y - camaraSizeY + margen;
        maxY = virtualCamera.transform.position.y + camaraSizeY - margen;
    }

    private void LimitarJugador()
    {
        //Clampea la posicion del jugador 

        foreach (GameObject g in getObjetosInteractuar())
        {
            Vector3 position = g.transform.position;

            position.x = Mathf.Clamp(position.x, minX, maxX);
            position.y = Mathf.Clamp(position.y, minY, maxY);

            g.transform.position = position;
        }

        Vector3 posicion = JugadorGameObject.transform.position;

        posicion.x = Mathf.Clamp(posicion.x, minX, maxX);
        posicion.y = Mathf.Clamp(posicion.y, minY, maxY);

        JugadorGameObject.transform.position = posicion;
    }

    #endregion

    #region Metodos de guardar estados para Control + Z
    public static void GuardarEstados()
    {
        //SI SE CREO ALGO EN UNA POSICION INTERMEDIA DE LOS ESTADOS DE CONTROL Z , BORRO TODO LO QUE ESTA DELANTE

        if (PosicionActual < CantidadEstados - 1)
        {
            foreach (GameObject g in getObjetosInteractuar())
            {
                ObjetoInteractuable objetoInteractuable = g.GetComponent<ObjetoInteractuable>();

                objetoInteractuable.ListaDeEstados.RemoveRange((PosicionActual + 1), CantidadEstados - (PosicionActual + 1));
            }
            CantidadEstados = PosicionActual + 1;
        }

        CantidadEstados++;
        PosicionActual++;

        foreach (GameObject g in getObjetosInteractuar())
        {
            //Busca todos los GameObjects con el componente objeto interacutable activo y les agrega un estado mas a la lista de estado
            //
            //Cada estado tiene:
            // -Si esta activo (sino lo esta desactivo su collider y su sprite pero el objeto sigue activo))
            // -Posicion
            // -Rotacion

            ObjetoInteractuable objetoInteractuable = g.GetComponent<ObjetoInteractuable>();

            EstadoObjeto estadoObjeto = new EstadoObjeto();
            estadoObjeto.estado = g.GetComponent<Collider2D>().isActiveAndEnabled;
            estadoObjeto.posicion = g.transform.position;
            estadoObjeto.rotacion = g.transform.rotation;

            objetoInteractuable.ListaDeEstados.Add(estadoObjeto);
        }

    }
    #endregion

    #region Metodos Vibrar Camara


    public static void VibrarCamara(float intensidad, float frecuencia, float tiempo)
    {
        CinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensidad;
        CinemachineBasicMultiChannelPerlin.m_FrequencyGain = frecuencia;
        intensidadInicial = intensidad;
        tiempoMovimiento = tiempo;
        tiempoMovimientoTotal = tiempo;
    }

    private void ModificacionVibracion()
    {
        if (tiempoMovimiento > 0)
        {
            tiempoMovimiento -= Time.deltaTime;
            CinemachineBasicMultiChannelPerlin.m_AmplitudeGain = Mathf.Lerp(intensidadInicial, 0, 1 - (tiempoMovimiento / tiempoMovimientoTotal));
        }
    }

    #endregion
}
