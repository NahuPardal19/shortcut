using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_MANAGER : MonoBehaviour
{
   private TextMeshProUGUI inputTxt;

    private void Start()
    {
        inputTxt = GameObject.Find("txtInputs").GetComponent<TextMeshProUGUI>();
    }
    private void Update()
    {
        MostrarInput();
    }

    private void MostrarInput()
    {
        inputTxt.text = "";


        foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        {

            if (Input.GetKey(keyCode))
            {

                inputTxt.text = "\n" + keyCode.ToString() + inputTxt.text ;
            }
        }
    }
}
