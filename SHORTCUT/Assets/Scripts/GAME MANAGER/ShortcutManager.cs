using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using System.Linq;
using Cinemachine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class ShortcutManager : MonoBehaviour
{
    #region PROPIEDADES GENERALES
    [Header("SHORTCUTS PERMITIDOS")]

    public static bool Can_Shortcut = true;

    [SerializeField] private bool Can_Copiar;
    [SerializeField] private bool Can_Cortar;
    [SerializeField] private bool Can_Zoom;
    [SerializeField] private bool Can_AltF4;
    [SerializeField] private bool Can_Control_A;
    [SerializeField] private bool Can_Control_Z;
    [SerializeField] private bool Can_Control_Y;
    [SerializeField] private bool Can_F5;
    [SerializeField] private bool Can_Control_F;
    [SerializeField] private bool Can_Control_D;
    [SerializeField] private bool Can_Control_O;
    [SerializeField] private bool Can_Tab;
    [SerializeField] private bool Can_Delete;
    [SerializeField] private bool Can_Control_S;

    #endregion


    #region Propiedades de Alt + F4
    [Header("ALT F4")]
    //Objetos que queremos eliminar al apretar ALT F4
    public GameObject[] ObjetosAEliminarAltF4;

    #endregion
    #region Propiedades del Zoom;
    public static float zoom = 0;

    private float ZoomCamaraOrtografica;

    [Header("VELOCIDAD ZOOM")]

    public float VelocidaddelZoomOrtografico;
    public float VelocidaddelZoomPerspectiva;

    [Header("ORTOGRAFICA")]
    public float limiteZoomMinOrtografica = 2;
    public float limiteZoomMaxOrtografica = 100;
    #endregion
    #region Propiedades del Control C/X
    private static List<GameObject> ObjetosCopiados;
    private static bool PuedePegar = false;
    private enum ModoControl { Copiar, Cortar };
    private static ModoControl modoControl;
    #endregion
    #region Propiedades Control Z
    public static bool PuedeControlZ;
    #endregion
    #region Propiedades Control F

    [Header("CONTROL F")]
    [SerializeField] private Color ColorObjetoSeleccionado_ControlF;
    [SerializeField] private Color ColorObjetoNoSeleccionado_ControlF;
    //Texto con la cantidad de objetos buscados y el seleccionados  EJ: 1/2
    private TextMeshProUGUI txtMany;

    //Posicion en la lista de objetos encontrados
    private int posicionEnLista;

    private GameObject InputField;
    private TMP_InputField InputFieldTxt;

    //Lista de objetos encontrados
    private List<GameObject> ObjetosEncontrados;


    #endregion
    #region Propiedades Tab
    private List<GameObject> ListaDeObjetosTab = new List<GameObject>();
    public int posicionActual;
    #endregion

    public static float distance = 10;


    [Header("PREFAB PUERTA")]
    public GameObject prefabPuerta;

    public static GameObject prefabPuertaStatic;

    public static List<GameObject> objetoSeleccionados;

    [Header("Sonidos")] 
    public AudioSource audioSource;
    public AudioClip sonidoPegar;
    public AudioClip sonidoPegar2;
    public AudioClip sonidoPegar3;
    public AudioClip noMasPegar;
    public AudioClip abrirPuerta;
    public AudioClip sonidoTab;
    public AudioClip sonidoControlZ;
    public AudioClip sonidoEliminar;
    
    [Header("Color Cortar Jugador")]
    public Color colorCortar;

    public bool estaColorRojoYnoEstaCortado;

    public static Color colorCortarStatic;

    void Start()
    {
        ZoomCamaraOrtografica = GameManager.virtualCamera.m_Lens.OrthographicSize;
        Can_Shortcut = true;
        InicializarAltF4();

        objetoSeleccionados = new List<GameObject>();
        ObjetosCopiados = new List<GameObject>();

        prefabPuertaStatic = prefabPuerta;

        InicializarControlF();

        ListaDeObjetosTab = new List<GameObject>();

        colorCortarStatic = colorCortar;

    }

    void Update()
    {

        if (Can_Shortcut)
            AllShortcut();

    }

    private void AllShortcut()
    {
        if (Can_AltF4)
            UsarAltF4();

        if (Can_Zoom)
            ZoomearCamaraOrtografica();

        if (Can_Copiar)
            Copiar();

        if (Can_Cortar)
            Cortar();

        //No les hace falta un if xq sino pueden copiar o cortar no pueden pegar

        Pegar();
        PegarCortar();


        if (Can_Control_A)
            ControlA();

        if (Can_Delete) Eliminar();


        if (Can_F5)
            ControlR_F5();

        if (Can_Control_F)
            ControlF();

        if (Can_Tab)
            MetodoTab();

        if (Can_Control_D)
            ControlD();

        if (Can_Control_O)
            Control_O();

        if (Can_Control_S)
            Control_S();

        if (Can_Control_Z)
            ControlZ();

        if (Can_Control_Y)
            ControlY();

        MantenerSeleccion();
        ActualizarLista();
    }

    #region Alt+F4
    [RuntimeInitializeOnLoadMethod]

    //DESACITVAR ALT F4

    static void RunOnStart()
    {
        Application.wantsToQuit += CallQuitWindow;
    }

    public static bool CallQuitWindow()
    {
        return false;
    }

    public static void Confirm()
    {
        Application.wantsToQuit -= CallQuitWindow;
        Application.Quit();
        print("juego cerrado");
    }

    private void UsarAltF4()
    {
        //Si se apreta ALT F4 se eliminan todos los GameObjetos de la lista de ObjetosAEliminarAltF4 


        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.F4))
        {
            var final = GameObject.Find("Final");
            if(final != null)final.GetComponent<Final>().startAnimation();
            
            print("VIKTOR");
            if (ObjetosAEliminarAltF4.Length >= 1)
            {
                Destroy(ObjetosAEliminarAltF4[0]);
                ObjetosAEliminarAltF4 = ObjetosAEliminarAltF4.Skip(1).ToArray();
                if (ObjetosAEliminarAltF4.Length <= 0)
                {
                    GameManager.MovimientoJugador(true);
                    ActivarAltF4.CanMoveAltF4 = true;
                }
            }
                
            
        }
    }

    private void InicializarAltF4()
    {
        //No se que hace esto la posta jaja , creo que es un hardcodeo del nivel de Altf4 para desactivar al cartel al inicio
        ObjetosAEliminarAltF4 = GameObject.FindGameObjectsWithTag("AltF4");
        //foreach (GameObject g in ObjetosAEliminarAltF4)
        //{
        //    g.SetActive(false);
        //}
    }

    #endregion

    #region METODOS COPY/PASTE
    public static void Copiar()
    {
        if (objetoSeleccionados != null)
        {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C))
            {
                //Esto es para que si esta en modo cortar y hago control c no se copien las monedas en trasnsparente ya que las devuelvo a su alpha normal
                if (modoControl == ModoControl.Cortar)
                {
                    foreach (GameObject g in objetoSeleccionados)
                    {
                        
                        SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                        Color color = sprite.color;
                        color.a = 1f;
                        sprite.color = color;
                        g.layer = LayerMask.NameToLayer("FloorInteractuable");
                    }
                }

                //Paso al modo copiar y digo que se puede pegar ya que tenemos algo copiado
                modoControl = ModoControl.Copiar;
                PuedePegar = true;

                //Borro toda la lista de objetos copiados y copio todos los objetos copiados (lo de la puerta es tuyo vos sabras)

                ObjetosCopiados.Clear();


                foreach (GameObject g in objetoSeleccionados)
                {
                    GameObject Gaux = g;
                    ObjetosCopiados.Add(Gaux);
                }
            }
        }

    }
    public static void Cortar()
    {
        if (objetoSeleccionados != null)
        {


            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.X))
            {
                //Borro la lista de objetos copiados (cortados y copiados entra en la misma lista) y digo que ya puede pegar

                PuedePegar = true;
                ObjetosCopiados.Clear();

                //Añado todos los objetos seleccionados y los "corto"

                foreach (GameObject g in objetoSeleccionados)
                {
                    ObjetosCopiados.Add(g);
                    if(g.GetComponent<ObjetoInteractuable>()!= null)
                    {
                        if(g.GetComponent<ObjetoInteractuable>().estaControlX == false)
                        {
                            g.GetComponent<ObjetoInteractuable>().estaControlX = true;
                        }

                    }

                    //g.GetComponent<SpriteRenderer>().color = colorCortarStatic;
                }

                //Paso a modo cortar

                modoControl = ModoControl.Cortar;

                //A los objetos seleccionados (cortados) les bajo el alfa para que esten medio transparentes

                foreach (GameObject g in ObjetosCopiados)
                {

                    SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                    Color color = sprite.color;
                    color.a = .7f;
                    sprite.color = color;
                    g.layer = LayerMask.NameToLayer("Cortado");
                }

            }
        }

    }
    private void PegarCortar()
    {
        if (modoControl == ModoControl.Cortar)
        {
            //Si el modo esta en cortar:
            

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V) && PuedePegar)
            {
                //Guardo un estado para el control z

                //Accedo a la posicion de mi mouse
                Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
                Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);



                //Si se pega mas de un objeto , por lo tanto se copio a mas de uno , los pego cerca de la posicion del mouse pero con un orden medianamente similar en el que estaban

                if (ObjetosCopiados.Count > 1)
                {
                    foreach (GameObject g in ObjetosCopiados)
                    {
                        
                        if (g.GetComponent<SpriteRenderer>().enabled)
                        {
                            if (GameManager.EstadoMouse)
                                g.transform.position += objPosition;

                            //Les saco la transparencia que le puse por el cortar
                            estaColorRojoYnoEstaCortado = false;
                            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;

                            SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                            Color color = sprite.color;
                            color.a = 1f;
                            sprite.color = color;

                            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
                            mMat.friction = 0;
                            g.GetComponent<Collider2D>().sharedMaterial = mMat;

                            g.layer = LayerMask.NameToLayer("FloorInteractuable");
                        }
                    }
                }

                //Si se pega solo un objeto lo pego en la posicion exacta del mouse
                else
                {
                    foreach (GameObject g in ObjetosCopiados)
                    {
                        if (g.GetComponent<ObjetoInteractuable>() != null)
                        {
                            if (g.GetComponent<ObjetoInteractuable>().estaControlX == true)
                            {
                                g.GetComponent<ObjetoInteractuable>().estaControlX = false;
                            }

                        }
                        if (g.GetComponent<SpriteRenderer>().enabled)
                        {
                            if (GameManager.EstadoMouse)
                                g.transform.position = objPosition;


                            //Le devuelvo la transparencia sacada por el cortar

                            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;

                            SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                            Color color = sprite.color;
                            color.a = 1f;
                            sprite.color = color;

                            //Le pongo el sprite sin outline
                            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;

                            g.layer = LayerMask.NameToLayer("FloorInteractuable");
                        }
                    }
                }

                GameManager.GuardarEstados();

            }
        }
    }
    private void Pegar()
    {
        
        //Si se copiaron los objetos
        if (modoControl == ModoControl.Copiar)
        {
            //Cuento la cantidad de objetos interactuables en la escena
            int contadorObjetos = 0;

            foreach (GameObject g in GameManager.getObjetosInteractuar())
            {
                if (g.GetComponent<ObjetoInteractuable>() != null)
                {
                    if (g.GetComponent<ObjetoInteractuable>().estaControlX == true)
                    {
                        g.GetComponent<ObjetoInteractuable>().estaControlX = false;
                    }

                }
                if (g.GetComponent<SpriteRenderer>().enabled) contadorObjetos++;
            }

          
            //Si no son menos de 3 objetos se pueden pegar

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V) && PuedePegar && contadorObjetos + ObjetosCopiados.Count <= GameManager.ObjetosInteractuablesMaximosStatic)
            {
    
                    foreach (GameObject g in ObjetosCopiados)
                    {
                        GameObject gAux;

                    if (g.GetComponent<ObjetoInteractuable>() != null)
                    {
                        if (g.GetComponent<ObjetoInteractuable>().estaControlX == true)
                        {
                            g.GetComponent<ObjetoInteractuable>().estaControlX = false;
                        }

                    }

                    gAux = Instantiate(g, g.transform.position, g.transform.rotation);
                        //Les pongo su color normal por las dudas
                        
                        reproducirSonidoAleatorio();

                        SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                        Color color = sprite.color;
                        color.a = 1f;
                        sprite.color = color;

                        //Les pongo el sprite sin seleccion 

                        gAux.GetComponent<SpriteRenderer>().enabled = true;
                        gAux.GetComponent<Collider2D>().enabled = true;

                        g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;
                        gAux.GetComponent<SpriteRenderer>().sprite = gAux.GetComponent<ObjetoInteractuable>().SpriteOutline;

                        PhysicsMaterial2D mMat = new PhysicsMaterial2D();
                        mMat.friction = 1;
                        gAux.GetComponent<Collider2D>().sharedMaterial = mMat;

                        g.layer = LayerMask.NameToLayer("FloorInteractuable");

                        gAux.GetComponent<ObjetoInteractuable>().ListaDeEstados = new List<EstadoObjeto>();

                        for (int i = 0; i < GameManager.CantidadEstados; i++)
                        {
                            gAux.GetComponent<ObjetoInteractuable>().ListaDeEstados.Add(new EstadoObjeto(Vector3.zero, Quaternion.identity, false));
                        }
                        //}

                    }

                //Busco con el control F ya que la cantidad de objetos encontrados puede cambiar
                //Por ejemplo: Si tenia un objeto llamado "Cubo" y yo busco eso , al copiarlo y pegarlo  habra otro mas llamado "Cubo"
                GameManager.GuardarEstados();
                BuscarControlF();
                ActualizarLista();
            }
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V) && PuedePegar && contadorObjetos + ObjetosCopiados.Count > GameManager.ObjetosInteractuablesMaximosStatic)
            {
                GameManager.VibrarCamara(10, 10, 0.5f);
                
                // sonido de que no se puede pegar
                audioSource.PlayOneShot(noMasPegar);
                
            }

        }

    }

    #endregion

    #region Metodos Zoom
    private void ZoomearCamaraOrtografica()
    {
        //EL AUMENTO A LA DISMINUCION DEL ZOOM EN ORTOGRAFICA VARIA CAMBIANDO EL ATRIBUTO DE LA CAMARA : ortographicSize

        //Con ruedita

        if (Input.GetAxis("Mouse ScrollWheel") > 0 && Input.GetKey(KeyCode.LeftControl) && GameManager.virtualCamera.m_Lens.OrthographicSize >= limiteZoomMinOrtografica)
        {
            ZoomCamaraOrtografica -= VelocidaddelZoomOrtografico;
            zoom += 0.1f;

        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && Input.GetKey(KeyCode.LeftControl) && GameManager.virtualCamera.m_Lens.OrthographicSize <= limiteZoomMaxOrtografica)
        {
            ZoomCamaraOrtografica += VelocidaddelZoomOrtografico;
            zoom -= 0.1f;
        }

        //Con + y -
        //Con + y -

        if (((Input.GetKeyDown(KeyCode.Plus) || ((Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Equals))) && Input.GetKey(KeyCode.LeftControl) && GameManager.virtualCamera.m_Lens.OrthographicSize >= limiteZoomMinOrtografica)))
        {
            ZoomCamaraOrtografica -= VelocidaddelZoomOrtografico;
            zoom += 0.1f;

        }
        if (((Input.GetKeyDown(KeyCode.Minus) || (Input.GetKeyDown(KeyCode.KeypadMinus)) && GameManager.virtualCamera.m_Lens.OrthographicSize <= limiteZoomMaxOrtografica)))
        {
            ZoomCamaraOrtografica += VelocidaddelZoomOrtografico;
            zoom -= 0.1f;
        }

        GameManager.virtualCamera.m_Lens.OrthographicSize = ZoomCamaraOrtografica;

    }


    #endregion

    #region Metodos Mover objeto

    //Estos metodo se llamara cuando drageo en el objeto interactualble


    //Este metodo se llamada cuando es mas de un objeto el que se mueve
    public static void MoverObjeto()
    {
        if (GameManager.EstadoMouse)
        {
            //Obtengo posicion del mouse
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

            //Muevo al objeto hacia la posicion del mouse

            foreach (GameObject g in objetoSeleccionados)
            {
                Rigidbody2D rb = g.GetComponent<Rigidbody2D>();
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                rb.MovePosition(objPosition);

                //Le cambio la layer a object para poder hacer que no colisione con el jugador
                g.layer = LayerMask.NameToLayer("Object");
            }
        }

    }
    //Este metodo se llamada cuando es un solo objeto el que se mueve
    public static void MoverObjeto(GameObject g)
    {
        if (GameManager.EstadoMouse)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

            Rigidbody2D rb = g.GetComponent<Rigidbody2D>();
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            rb.MovePosition(objPosition);
            g.GetComponent<Collider2D>().isTrigger = false;
        }
    }

    #endregion

    #region Metodos Eliminar

    private void Eliminar()
    {
        if ((Input.GetKeyDown(KeyCode.Delete)|| Input.GetKeyDown(KeyCode.Backspace))&& objetoSeleccionados != null)
        {
            //Guardo un estado al control z

            //No elimino el objeto sino que le quito Sprite y collider
            foreach (GameObject g in objetoSeleccionados)
            {
                g.GetComponent<SpriteRenderer>().enabled = false;
                g.GetComponent<Collider2D>().enabled = false;
                if(g.GetComponent<Light2D>())
                g.GetComponent<Light2D>().enabled = false;
            }

            if (objetoSeleccionados.Count > 0)
            {
                GameManager.GuardarEstados();
                audioSource.PlayOneShot(sonidoEliminar);
            }

            //Quito toda seleccion para que los objetos eliminados , que estaban seleccionados , pierdan esa seleccion sino bugs jej
            DeseleccionarTodo();

            if (posicionEnLista != 1)
                CambiarPosicionEnLista(false);

            BuscarControlF();


            ActualizarLista();


        }
    }

    #endregion

    #region Metodo Seleccionar Objeto

    //SELECCION DE UN SOLO OBJETO
    public static void SeleccionarObjeto(GameObject gPar)
    {
        //Borro toda seleccion y añado el objeto seleccionado a la lista de seleccionados
        objetoSeleccionados.Clear();
        objetoSeleccionados.Add(gPar);

        //Le pongo el sprite de seleccionado
        foreach (GameObject g in objetoSeleccionados)
        {
            if (g.GetComponent<Moneda>() || g.CompareTag("Player") )
            {
                g.GetComponent<Animator>().enabled= false;
            }
            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().SpriteOutline;
            g.GetComponent<PlayerMovement>().enabled = true;

            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
            mMat.friction = 0;
            g.GetComponent<Collider2D>().sharedMaterial = mMat;
        }

        //Elimino toda seleccion ya que en este caso solo podra haber un objeto seleccionado , sino se acumularian
        EliminarSelecciones();

    }

    //SELECCION DE UN OBJETO PERO CON CONTROL + CLICK PARA PODER SELECCIONAR A MAS DE UNO
    public static void SeleccionarOtroObjetoMas(GameObject gPar)
    {
        //No limpio la lista porque en este caso se iran acumulando
        objetoSeleccionados.Add(gPar);

        foreach (GameObject g in objetoSeleccionados)
        {
            if (g.GetComponent<Moneda>() || g.CompareTag("Player"))
            {
                g.GetComponent<Animator>().enabled= false;
            }
            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().SpriteOutline;


            g.GetComponent<PlayerMovement>().enabled = true;
            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
            mMat.friction = 0;
            g.GetComponent<Collider2D>().sharedMaterial = mMat;
        }


        EliminarSelecciones();
    }

    //MANTIENE EL OUTLINE EN LOS OBJETOS SELECCIONADOS QUE SE USA EN EL UPDATE PARA QUE MANTENGAN SU SPRITE (HABRA QUE VER SI SE PUEDE QUITAR)
    private void MantenerSeleccion()
    {
        if (objetoSeleccionados != null)
        {

            foreach (GameObject g in objetoSeleccionados)
            {
                if (g != null)
                {
                    if (g.GetComponent<Moneda>() || g.CompareTag("Player"))
                    {
                        g.GetComponent<Animator>().enabled= false;
                    }
                    g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().SpriteOutline;
                    
                    g.GetComponent<PlayerMovement>().enabled = true;
                    PhysicsMaterial2D mMat = new PhysicsMaterial2D();
                    mMat.friction = 0;
                    g.GetComponent<Collider2D>().sharedMaterial = mMat;
                }

            }
        }

    }

    //ELIMINO SPRITE CON OUTLINE A TODO OBJETO , LO USO EN EL ELIMINAR
    private static void EliminarSelecciones()
    {
        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;
            g.GetComponent<PlayerMovement>().enabled = false;
            g.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            if((g.CompareTag("Player") || g.GetComponent<Moneda>())&& !objetoSeleccionados.Contains(g)) g.GetComponent<Animator>().enabled = true;

            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
            mMat.friction = 1;
            g.GetComponent<Collider2D>().sharedMaterial = mMat;
        }
    }

    //DESELECCIONO TODOS LOS OBJETOS Y TAMBIEN LES QUITO EL OUTLINE
    private static void DeseleccionarTodo()
    {
        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;
            g.GetComponent<PlayerMovement>().enabled = false;
            g.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
            mMat.friction = 1;
            if (g.CompareTag("Player") || g.GetComponent<Moneda>()) g.GetComponent<Animator>().enabled = true;
            g.GetComponent<Collider2D>().sharedMaterial = mMat;
        }
        objetoSeleccionados.Clear();
    }

    //SU FUNCION ES SELECCIONAR TODOS LOS OBJETOS SELECCIONADOS , EN ESTE CASO SERA SOLO CUANDO EL USUARIO APRETE CONTROL A
    private static void SeleccionarObjeto(List<GameObject> objetos)
    {
        objetoSeleccionados.Clear();

        foreach (GameObject g in objetos)
        {
            objetoSeleccionados.Add(g);
            PhysicsMaterial2D mMat = new PhysicsMaterial2D();
            mMat.friction = 0;
            g.GetComponent<Collider2D>().sharedMaterial = mMat;
        }
        EliminarSelecciones();
    }

    #endregion

    #region Metodo Control A

    private void ControlA()
    {
        //AGARRA TODOS LOS OBJETOS INTERACTUABLES ACTIVOS Y LOS SELECCIONA
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.A))
            SeleccionarObjeto(GameManager.getObjetosInteractuar());
    }

    #endregion

    #region Metodo Control Z + Control Y

    private void ControlZ()
    {
        if (Input.GetKeyDown(KeyCode.Z) && Input.GetKey(KeyCode.LeftControl))
        {
            //Si se puede hacer control z (si hay acciones a las que pueda hacer undo)
            //recorro todos los objetos interactuables
            

            if (GameManager.PosicionActual != 0)
            {
                GameManager.PosicionActual--;
                foreach (GameObject g in GameManager.getObjetosInteractuar())
                {
                    
                    //Obtengo el estado al que voy a ir y lo elimino
                    EstadoObjeto estado = g.GetComponent<ObjetoInteractuable>().ListaDeEstados[GameManager.PosicionActual];

                    //Paso es estado al objeto actual
                    g.GetComponent<SpriteRenderer>().enabled = estado.estado;
                    g.GetComponent<Collider2D>().enabled = estado.estado;

                    if (g.GetComponent<Light2D>())
                        g.GetComponent<Light2D>().enabled = estado.estado;

                    g.transform.position = estado.posicion;
                    g.transform.rotation = estado.rotacion;
                }
            
                BuscarControlF();
                DeseleccionarTodo();
                ActualizarLista();
            }


        }
    }

    private void ControlY()
    {
        if (Input.GetKeyDown(KeyCode.Y) && Input.GetKey(KeyCode.LeftControl))
        {
            //Si se puede hacer control z (si hay acciones a las que pueda hacer undo)
            //recorro todos los objetos interactuables


            if (GameManager.PosicionActual != GameManager.CantidadEstados - 1)
            {
                GameManager.PosicionActual++;
                foreach (GameObject g in GameManager.getObjetosInteractuar())
                {
                    //Obtengo el estado al que voy a ir y lo elimino
                    EstadoObjeto estado = g.GetComponent<ObjetoInteractuable>().ListaDeEstados[GameManager.PosicionActual];

                    //Paso es estado al objeto actual
                    g.GetComponent<SpriteRenderer>().enabled = estado.estado;
                    g.GetComponent<Collider2D>().enabled = estado.estado;

                    if (g.GetComponent<Light2D>())
                        g.GetComponent<Light2D>().enabled = estado.estado;

                    g.transform.position = estado.posicion;
                    g.transform.rotation = estado.rotacion;

                }
            }
             
            audioSource.PlayOneShot(sonidoControlZ);
            
            BuscarControlF();
            DeseleccionarTodo();
            ActualizarLista();
        }


    }






    #endregion

    #region Metodo Control R & F5

    private void ControlR_F5()
    {
        //VIKTOR
        if ((Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R)) || Input.GetKeyDown(KeyCode.F5))
        {
            GameManager.ReiniciarNivel();
        }
    }

    #endregion

    #region Metodo Control F
    private void InicializarControlF()
    {
        InputField = GameObject.Find("InputField (TMP)");
        txtMany = GameObject.Find("Many").GetComponent<TextMeshProUGUI>();
        InputFieldTxt = InputField.GetComponent<TMP_InputField>();
        InputField.SetActive(false);
        ObjetosEncontrados = new List<GameObject>();

    }
    private void ControlF()
    {
        //Activa el input field
        if ((Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.F)))
        {
            if (!InputField.activeInHierarchy)
            {
                InputField.SetActive(true);
                InputFieldTxt.text = "";
                GameManager.MovimientoJugador(false);
                InputFieldTxt.ActivateInputField();
            }
            else
            {
                InputFieldTxt.DeactivateInputField();
                InputField.SetActive(false);
                GameManager.MovimientoJugador(true);
            }
        }
        if (Input.GetKey(KeyCode.Escape) && InputField.activeSelf == true)
        {
            InputFieldTxt.DeactivateInputField();
            InputField.SetActive(false);
            GameManager.MovimientoJugador(true);
        }

        //if (Input.GetKeyDown(KeyCode.Return))
        //{
        //    if (!InputField.activeInHierarchy)
        //    {
        //        InputField.SetActive(true);
        //        InputFieldTxt.text = "";
        //        GameManager.MovimientoJugador(false);
        //        InputFieldTxt.ActivateInputField();
        //    }
        //}

    }
    public void BuscarControlF()
    {

        ObjetosEncontrados.Clear();

        //Agregar objetos encontrados a una lista

        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {

            if (g.GetComponent<ObjetoInteractuable>().Nombre.ToLower() == InputFieldTxt.text.ToLower() && g.GetComponent<SpriteRenderer>().enabled)
            {
                ObjetosEncontrados.Add(g);
            }
        }

        //Mostrar cantidad de objetos encontrados

        //if (posicionEnLista == 0) posicionEnLista = 1;

        //txtMany.text = $"{posicionEnLista}/{ObjetosEncontrados.Count}";


        if (ObjetosEncontrados.Count <= 0) txtMany.text = "0/0";

        MarcarObjetos();

    }
    public void CambiarPosicionEnLista(bool down)
    {
        //Es eñ encargado de cambiar de posicion de los objetos encontrados en el control f 

        if (ObjetosEncontrados.Count > 0)
        {
            if (down)
            {


                if (ObjetosEncontrados.Count > posicionEnLista)
                {
                    posicionEnLista++;
                }
                else
                {
                    posicionEnLista = 1;
                }
            }

            else
            {
                if (posicionEnLista > 1)
                {
                    posicionEnLista--;
                }
                else
                {
                    posicionEnLista = ObjetosEncontrados.Count;
                }
            }

            //txtMany.text = $"{posicionEnLista}/{ObjetosEncontrados.Count}";
            //if (ObjetosEncontrados.Count <= 0) txtMany.text = "0/0";
            MarcarObjetos();

        }
    }
    public void MarcarObjetos()
    {
        //El objeto en la lista de posicion seleccionadas se pondra en rojo y el resto en amarillo 

        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            
            if(!g.CompareTag("Player")) g.GetComponent<SpriteRenderer>().color = GameManager.materialObjetosInteractuableStatic;

        }
        if (ObjetosEncontrados.Count > 0 && InputField.activeInHierarchy)
        {
            objetoSeleccionados.Clear();
            foreach (GameObject g in ObjetosEncontrados)
            {
                g.GetComponent<SpriteRenderer>().color = ColorObjetoSeleccionado_ControlF;
                objetoSeleccionados.Add(g);
            }

            //ObjetosEncontrados[posicionEnLista - 1].GetComponent<SpriteRenderer>().color = ColorObjetoSeleccionado_ControlF;
            //SeleccionarObjeto(ObjetosEncontrados[posicionEnLista - 1]);
        }
    }
    public void QuitarInputField()
    {
        InputField.SetActive(false);
        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            if (!g.CompareTag("Player")) g.GetComponent<SpriteRenderer>().color = GameManager.materialObjetosInteractuableStatic;

        }
    }

    #endregion

    #region Metodo Control D

    private void ControlD()
    {
        int contadorObjetos = 0;
        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            if (g.GetComponent<SpriteRenderer>().enabled) contadorObjetos++;
        }

        //

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D) && objetoSeleccionados.Count > 0 && contadorObjetos + objetoSeleccionados.Count <= GameManager.ObjetosInteractuablesMaximosStatic)
        {
            GameObject gAux;

            foreach (GameObject g in objetoSeleccionados)
            {
                gAux = Instantiate(g, g.transform.position, Quaternion.identity);


                reproducirSonidoAleatorio();
                
                SpriteRenderer sprite = g.GetComponent<SpriteRenderer>();
                Color color = sprite.color;
                color.a = 1f;
                sprite.color = color;

                PhysicsMaterial2D mMat = new PhysicsMaterial2D();
                mMat.friction = 1;
                gAux.GetComponent<Collider2D>().sharedMaterial = mMat;

                g.layer = LayerMask.NameToLayer("FloorInteractuable");
                gAux.layer = LayerMask.NameToLayer("FloorInteractuable");

                g.GetComponent<SpriteRenderer>().sprite = g.GetComponent<ObjetoInteractuable>().Sprite;
                gAux.GetComponent<SpriteRenderer>().sprite = gAux.GetComponent<ObjetoInteractuable>().SpriteOutline;
                gAux.GetComponent<ObjetoInteractuable>().ListaDeEstados = new List<EstadoObjeto>();

                //SeleccionarObjeto(gAux);

                for (int i = 0; i < GameManager.CantidadEstados; i++)
                {
                    gAux.GetComponent<ObjetoInteractuable>().ListaDeEstados.Add(new EstadoObjeto(Vector3.zero, Quaternion.identity, false));
                }


            }
            GameManager.GuardarEstados();
            BuscarControlF();
            ActualizarLista();

        }
        else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D) && objetoSeleccionados.Count > 0 && contadorObjetos + objetoSeleccionados.Count > GameManager.ObjetosInteractuablesMaximosStatic)
        {
            print("d");
            GameManager.VibrarCamara(10, 10, 0.5f);
            audioSource.PlayOneShot(noMasPegar);
        }
    }

    #endregion

    #region Metodo Tab

    private void ActualizarLista()
    {
        ListaDeObjetosTab.Clear();
        foreach (GameObject g in GameManager.getObjetosInteractuar())
        {
            if (g.GetComponent<SpriteRenderer>().enabled)
                ListaDeObjetosTab.Add(g);
        }
    }

    private void MetodoTab()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (posicionActual < ListaDeObjetosTab.Count)
            {
                SeleccionarObjeto(ListaDeObjetosTab[posicionActual]);
                posicionActual++;
                audioSource.PlayOneShot(sonidoTab);
            }
            else if (ListaDeObjetosTab.Count < 1)
            {
                return;
            }
            else
            {
                audioSource.PlayOneShot(sonidoTab);
                posicionActual = 0;
                SeleccionarObjeto(ListaDeObjetosTab[posicionActual]);
                posicionActual++;
            }

        }
    }

    #endregion

    #region Metodo Control + O

    private void Control_O()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.O))
        {
            FindObjectOfType<Door>().AbrirPuerta();
        }
    }

    #endregion

    #region Metodo Control + S

    private void Control_S()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            GameManager.GuardarEstados();
        }
    }

    #endregion

    public void reproducirSonidoAleatorio()
    {
        int randomNumber = Random.Range(1, 4);
        if(randomNumber == 1){ audioSource.PlayOneShot(sonidoPegar);}
        else if(randomNumber == 2){ audioSource.PlayOneShot(sonidoPegar2);}
        else if(randomNumber == 3){ audioSource.PlayOneShot(sonidoPegar3);}
    }
}



