using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Final : MonoBehaviour
{
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void startAnimation()
    {
        _animator.SetBool("End", true);
    }

    public void closeGame()
    {
        ShortcutManager.Confirm();
    }
}
