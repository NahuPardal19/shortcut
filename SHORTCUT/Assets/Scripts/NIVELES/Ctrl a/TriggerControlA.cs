using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerControlA : MonoBehaviour
{

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager g = FindObjectOfType<GameManager>();
            g.timerActivado = true;
            GameObject obstaculos = GameObject.Find("Obstaculos");
            obstaculos.transform.position = obstaculos.transform.position + (Vector3.up * 15);
            Destroy(gameObject);
        }
    }
}

