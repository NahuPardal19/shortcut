using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ContadorCubos : MonoBehaviour
{
    public int contador = 0;
    public static int contadorStatic = 0;
    public int Maximo;
    public GameObject obstaculo;

    public GameObject paredActivar;
    public GameObject paredDesactivar;

    public bool nivleControlz = false;


    void Start()
    {
        contador = 0;
        contadorStatic = contador;
    }


    void Update()
    {
        if (Maximo == 1)
        {
            if (contador == Maximo || contador > Maximo)
            {
                obstaculo.SetActive(false);
            }
            else if (contador != Maximo)
            {
                if (!obstaculo.activeSelf)
                {
                    obstaculo.SetActive(true);
                }
            }
        }

        if (paredActivar != null && nivleControlz)
        {
            if (Maximo == 3 && contador == Maximo)
            {
                paredActivar.SetActive(true);
                paredDesactivar.SetActive(false);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Contador"))
        {
            contador++;
            Balanza.numeroBalanza = contador;
        }


    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Contador"))
        {
            contador--;
            Balanza.numeroBalanza = contador;
        }
    }

    private void OnDestroy()
    {
    }

}

