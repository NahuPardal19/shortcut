using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelDoor : MonoBehaviour
{
    private RawImage image;

    //private Color colorInicio;
    [SerializeField] private Color colorCorrecta;

    //private bool clickeado = false;

    [SerializeField]private TextMeshPro text;
    private bool PuertaAbierta;
    [SerializeField] private string Password;

    private void Start()
    {
        image = GetComponent<RawImage>();
        //colorInicio = image.color;
    }

    private void Update()
    {
        if(!PuertaAbierta)
        LeerNumero();
        VerificarPassWord();
    }

    private void PonerNumero(string n)
    {
        if(text.text.Length < 4)
        {
            text.text += n;
        }
        else
        {
            text.text = n;
        }
    }
    private void LeerNumero()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0))
        {
            PonerNumero("0");
        }
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            PonerNumero("1");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            PonerNumero("2");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            PonerNumero("3");
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
        {
            PonerNumero("4");
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5))
        {
            PonerNumero("5");
        }
        if (Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6))
        {
            PonerNumero("6");
        }
        if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7))
        {
            PonerNumero("7");
        }
        if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8))
        {
            PonerNumero("8");
        }
        if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9))
        {
            PonerNumero("9");
        }
    }
    private void VerificarPassWord()
    {
        if(Password == text.text)
        {
            GameObject g = GameObject.Find("Obstaculo");
            if(g != null) g.gameObject.SetActive(false);
            image.color = colorCorrecta;
        }
    }

    //private void OnMouseDown()
    //{
    //    image.color = colorCambio;
    //    clickeado = true;
    //}
    //private void OnMouseEnter()
    //{
    //    image.color = colorCambio;
    //}
    //private void OnMouseExit()
    //{
    //    if (!clickeado) image.color = colorInicio;
    //}
}
