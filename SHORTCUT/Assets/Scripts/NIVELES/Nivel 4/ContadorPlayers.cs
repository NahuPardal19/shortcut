using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContadorPlayers : MonoBehaviour
{
    public static int contador = 0;
    [SerializeField] TextMeshPro txtcont;

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            contador++;
            //collision.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            //collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            txtcont.text = contador.ToString() + "/1";
        }


    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            contador--;
            //collision.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            //collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
            txtcont.text = contador.ToString() + "/1";
        }
    }
}
