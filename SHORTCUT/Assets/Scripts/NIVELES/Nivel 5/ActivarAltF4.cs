using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class ActivarAltF4 : MonoBehaviour
{
    public Animator animator;
    static public bool CanMoveAltF4 = true;
    public List<GameObject> ObjectsToPaint;
    public Color color;
    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            animator.SetBool("inside", true);
            GameManager.MovimientoJugador(false);
            CanMoveAltF4 = false;
            GetComponent<Collider2D>().enabled = false;
            //GetComponent<SpriteRenderer>().enabled = true;

            if (SceneManager.GetActiveScene().buildIndex == 10)
            {
                print(name);
                foreach (GameObject g in ObjectsToPaint)
                {
                    if(g.GetComponent<SpriteRenderer>())g.GetComponent<SpriteRenderer>().color = color;
                    if(g.GetComponent<Tilemap>()) g.GetComponent<Tilemap>().color = color;
                }
            }
       }
    }
}
