using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlX_Trigger : MonoBehaviour
{
    public bool PuertaActivada = true;
    private GameObject puerta;
    private bool PuertaAbierta;


    void Start()
    {
        puerta = GameObject.Find("puerta");    
    }

    void Update()
    {
        if (PuertaActivada)
        {
            if (!PuertaAbierta)
            {
                puerta.transform.position += Vector3.up * 70;
                PuertaAbierta = true;
            }

        }
        else
        {
            if (PuertaAbierta)
            {
                puerta.transform.position -= Vector3.up * 70;
                PuertaAbierta = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<ObjetoInteractuable>().estaControlX)
        {
            PuertaActivada = false;
        }
        else
        {
            PuertaActivada = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.gameObject.GetComponent<ObjetoInteractuable>().estaControlX)
        {
            PuertaActivada = false;
        }
        else
        {
            PuertaActivada = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PuertaActivada = true;
    }


}
