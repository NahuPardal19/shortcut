using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

public class IbotonPresionable : MonoBehaviour
{
    public bool isPressed;
    private Renderer component;
    public Material materialPresionado;
    private Material materialOriginal;

    [FormerlySerializedAs("barrera")] public GameObject barreraBotonOn;
    public GameObject barreraBotonOff;

    private void Start()
    {
        component = GetComponent<Renderer>();
        materialOriginal = component.material;
    }

    public void onPress()
    {
        if (component != null && materialPresionado != null)
        {
            component.material = materialPresionado;
            barreraBotonOn.gameObject.SetActive(false);
            if (barreraBotonOff != null)
            {
                barreraBotonOff.gameObject.SetActive(true);
            }
        }
        else Debug.Log("Falta asignar material");
    }

    public void onExit()
    {
        if (component != null && materialPresionado != null)
        {
            component.material = materialOriginal;
            barreraBotonOn.gameObject.SetActive(true);
            if (barreraBotonOff != null)
            {
                barreraBotonOff.gameObject.SetActive(false);
            }
        }
    }
}
