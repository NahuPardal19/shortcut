using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    private GameObject menu;
    private GameObject postProcessing;
    public Animation animation;
    public AnimationClip openAnimation;
    public AnimationClip closeAnimation;

    private void Start()
    {
        postProcessing = GameObject.Find("Global Volume");
        menu = gameObject;
    }

    public void OpenAnimation()
    {
        animation.clip=openAnimation;
        animation.Play();
    }

    public void CloseAnimation()
    {
        animation.clip=closeAnimation;
        animation.Play();
    }
    public void Resume()
    {
        print("fdaf");
        menu.SetActive(false);
        postProcessing.SetActive(false);
        if (ActivarAltF4.CanMoveAltF4)
        {
            GameManager.MovimientoJugador(true);
        }
        ShortcutManager.Can_Shortcut = true;
        GameManager.ActivarMouse(false);
    }

    public void QuitGame()
    {
        ShortcutManager.Confirm();
    }

    public void FirstLevel()
    {
        SceneManager.LoadScene(0);
    }
}
