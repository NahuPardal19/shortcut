using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloquearMouse : MonoBehaviour
{
    void Update()
    {
        Vector3 posicionMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(posicionMouse, Vector2.zero);

        if (hit.collider != null)
        {
            // El mouse est� sobre un collider, puedes realizar acciones aqu�
            // Por ejemplo, puedes impedir que el jugador se mueva hacia esa direcci�n
            Debug.Log("Mouse sobre zona bloqueada");
        }
        else
        {
            // El mouse no est� sobre un collider, puedes permitir el movimiento
        }
    }
}
